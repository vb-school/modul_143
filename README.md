# Modul 143 - Antliuma Aerospace Laboratories (AAL) - Backup@birdCloud
## Valentin Binotto

**Die im Laufe des Moduls 143 verwendete fiktive Firma, Antliuma Aerospace Laboratories (AAL), ist auch Gegenstand des Moduls 346. Sämtliche Informationen zur gesamten Cloud-Umgebung von AAL, inklusive dem Backup-/Restoresystem, finden sich in meinem [GIT Repo zum Modul 346](https://gitlab.com/vb-school/modul_346/). Die für das Modul 143 relevanten Inhalte finden sich folglich im GIT Repo zum Modul 346 insbesondere in den folgenden Abschnitten der Dokumentation:**



- [Abschnitt Datenerhaltung & Wiederherstellungsschutzgrad - Backup-Systeme (auch Modul 143)](https://gitlab.com/vb-school/modul_346/-/blob/main/Dokumentationen/Project_birdCloud.md#datenerhaltung-wiederherstellungsschutzgrad-backup-systeme-auch-modul-143)
- [Abschnitt Planung Backup-Systeme](https://gitlab.com/vb-school/modul_346/-/blob/main/Dokumentationen/Project_birdCloud.md#backup-systeme-auch-modul-143)
- [Praktische Umsetzung Backup-Systeme](https://gitlab.com/vb-school/modul_346/-/blob/main/Dokumentationen/Project_birdCloud.md#einrichtung-und-konfiguration-backup-system-auch-modul-143)
